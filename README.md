# dotfiles

My dotfiles managed with [GNU Stow](https://www.gnu.org/software/stow/)

## How to use

### Creating dotfiles

Create a git repository.

```bash
$ mkdir ~/.dotfiles
$ cd ~/.dotfiles
$ git init .
$ echo "This is a dotfiles repo" > README.md
$ git add README.md
$ git commit -m "Create dotfiles repository"
```

Inside .dotfiles, create folders with the same structure that is in your system.

```bash
$ cd ~/.dotfiles
$ mkdir -p nvim/.config/nvim
$ touch nvim/.config/nvim/init.vim
$ mkdir -p ssh/.ssh
$ touch ssh/.ssh/config
$ mkdir zsh
$ touch zsh/.zshrc

```

We have this structure in our .dotfiles folder.

```bash
$ tree -a .dotfiles
.dotfiles
├── nvim
│   └── .config
│       └── nvim
│           └── init.vim
├── ssh
│   └── .ssh
│       └── config
└── zsh
    └── .zshrc
```

Now we use Stow to move our config files to the .dotfiles folder and create symlinks to the original place. First we use the `-n` flag to preview the action and `-v` verbose mode.

```bash
$ stow --adopt -nv nvim
MV: .config/nvim/init.vim -> .dotfiles/nvim/.config/nvim/init.vim
LINK: .config/nvim/init.vim => ../../.dotfiles/nvim/.config/nvim/init.vim
WARNING: in simulation mode so not modifying filesystem.
```

To perform the action we run the same instruction without the `-n` flag.

### Restoring dotfiles in a new system

To restore your dotfiles in a new system you can follow these steps:

```bash
$ git clone your_repository ~/.dotfiles
$ cd ~/.dotfiles
$ stow -v --stow ssh
```
